package com.gitlab.rstadeu.automation.cucumber.test.steps;

import com.gitlab.rstadeu.automation.cucumber.context.TestContext;
import com.gitlab.rstadeu.automation.cucumber.helpers.JsHelper;
import com.gitlab.rstadeu.automation.cucumber.helpers.WaitHelper;
import com.gitlab.rstadeu.automation.cucumber.pageobjects.FacebookPage;
import com.gitlab.rstadeu.automation.cucumber.messages.Messages;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.WebDriver;
import static org.assertj.core.api.Assertions.*;

public class ApplicationSteps {
  private WebDriver driver;
  private WaitHelper waitHelper;
  private JsHelper jsHelper;
  private FacebookPage facebookPage;

  public ApplicationSteps(TestContext context) {
    driver = context.getDriverManager().getDriver();
    waitHelper = context.getHelperManager().getWaitHelper();
    jsHelper = context.getHelperManager().getJsHelper();
    facebookPage = context.getPageObjectManager().getFacebookPage();
  }

  @Given("^I go to facebook page$")
  public void I_go_to_facebook_page() {
    driver.navigate().to("https://facebook.com");
  }

  @When("^I click on email and enter \"([^\"]*)\"$")
  public void I_click_on_email_and_enter(String email) {
    facebookPage.emailTxt.sendKeys(email);
  }

  @When("^I click on password and enter \"([^\"]*)\"$")
  public void I_click_on_password_and_enter(String password) {
    facebookPage.passwordTxt.sendKeys(password);
  }

  @When("^I click on logging button$")
  public void I_click_on_logging_button() {
    facebookPage.loginButton.click();
  }

  @Then("^I should be redirect to login error page$")
  public void I_should_be_redirect_to_login_error_page() {
    String result = facebookPage.headTitle.getText();
    assertThat(result).isEqualToIgnoringCase(Messages.ENTRAR_NO_FACEBOOK);
  }
}
