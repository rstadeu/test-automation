package com.gitlab.rstadeu.automation.cucumber.test.runners;

import com.cucumber.listener.Reporter;
import com.gitlab.rstadeu.automation.cucumber.initialize.TestValues;
import cucumber.api.CucumberOptions;
import cucumber.api.java.After;
import cucumber.api.testng.AbstractTestNGCucumberTests;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;

import java.io.File;

@RunWith(Parameterized.class)
@CucumberOptions(
        features = {"classpath:features"},
        glue = {"classpath:com.gitlab.rstadeu.automation.cucumber.test.steps"},
        plugin = {"com.cucumber.listener.ExtentCucumberFormatter:target/cucumber-reports/MainReport.html",
            "json:target/cucumber-reports/MainReport.json"}
)
public class ApplicationRunner extends AbstractTestNGCucumberTests {
    @After(order = 2)
    public static void teardown(){
        Reporter.loadXMLConfig(new File("src/test/resources/extent-report.xml"));
    }

    @Parameters({"browser"})
    @BeforeClass
    public void beforeClass(String browser){
        TestValues.setBrowser(browser);
    }
}
