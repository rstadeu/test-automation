package com.gitlab.rstadeu.automation.cucumber.test.steps;

import com.cucumber.listener.Reporter;
import com.gitlab.rstadeu.automation.cucumber.helpers.LoggerHelper;
import com.gitlab.rstadeu.automation.cucumber.context.TestContext;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.apache.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.reporters.Files;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Hooks {
  private static Logger log = LoggerHelper.getLogger(Hooks.class);
  TestContext testContext;

  public Hooks(TestContext context) {
    testContext = context;
  }

  @Before
  public void beforeScenario() {}

  @After(order = 0)
  public void quitDriver(Scenario scenario) {
    log.info("Close driver");
    testContext.getDriverManager().quitDriver();
  }

  @After(order = 1)
  public void afterScenario(Scenario scenario) {
    if (scenario.isFailed()) {
        log.info("Test Failed! Taking screenshot from the page.");
      String screenshotName = scenario.getName().replaceAll(" ", "_");
      Calendar calendar = Calendar.getInstance();
      SimpleDateFormat formatter = new SimpleDateFormat("dd_MMM_yyyy_HH_mm_ss");
      try {
        TakesScreenshot takesScreenshot =
            (TakesScreenshot) testContext.getDriverManager().getDriver();
        File sourcePath = takesScreenshot.getScreenshotAs(OutputType.FILE);
        FileInputStream fileInputStream = new FileInputStream(sourcePath);
        File destinationFilePath =
            new File(
                System.getProperty("user.dir")
                    + "/target/cucumber-reports/screenshots/"
                    + screenshotName
                    + "_"
                    + formatter.format(calendar.getTime())
                    + ".png");
        Files.copyFile(fileInputStream, destinationFilePath);
          Reporter.addScreenCaptureFromPath(destinationFilePath.toString());
      } catch (IOException e) {
          log.error("Couldn't save the image. Error message:\n" + e.getMessage(), e);
      }
    }
  }
}
