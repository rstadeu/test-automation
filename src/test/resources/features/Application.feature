Feature: Verify first page of Facebook

  @FacebookFrontPage
  Scenario Outline: Verify the front page
    Given I go to facebook page
    When I click on email and enter "<Email>"
    And I click on password and enter "<Password>"
    And I click on logging button
    Then I should be redirect to login error page
    Examples:
      | Email                       | Password      |
      | asdasda@asdasd.com          | asdasdasdasda |
      | rwrewwerw@ljdnjd.com        | ornosjdnfosjd |
      | porinmmifmrif@dkdfjdfjd.com | pdoskdoaskods |