package com.gitlab.rstadeu.automation.cucumber.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FacebookPage {

  @FindBy(xpath = "//input[@id=\"email\"]")
  public WebElement emailTxt;

  @FindBy(xpath = "//input[@id=\"pass\"]")
  public WebElement passwordTxt;

  @FindBy(xpath = "//label[@id=\"loginbutton\"]/input")
  public WebElement loginButton;

  @FindBy(css = "div._4rbf._53ij > a")
  public WebElement popUpMessage;

  @FindBy(xpath = "//div[@id=\"header_block\"]/span")
  public WebElement headTitle;

  private WebDriver driver;

  public FacebookPage(WebDriver driver) {
    this.driver = driver;
    PageFactory.initElements(driver, this);
  }
}
