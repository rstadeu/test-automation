package com.gitlab.rstadeu.automation.cucumber.enums;

public enum BrowserType {
  CHROME,
  IE,
  FIREFOX,
  PHANTOMJS,
  SAFARI
}
