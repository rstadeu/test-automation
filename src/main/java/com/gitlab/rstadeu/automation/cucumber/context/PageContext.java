package com.gitlab.rstadeu.automation.cucumber.context;

public enum PageContext {
  ID,
  NAME,
  FIELD
}
