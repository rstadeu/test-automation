package com.gitlab.rstadeu.automation.cucumber.helpers;

import org.apache.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;


public class AngularHelper {
  private  WebDriver jsDriver;
  private  WebDriverWait jsWait;
  private  JavascriptExecutor jsExec;
  private static Logger log = LoggerHelper.getLogger(AngularHelper.class);

  public AngularHelper(WebDriver driver){
    setDriver(driver);
  }
  public void setDriver(WebDriver driver) {
    jsDriver = driver;
    jsWait = new WebDriverWait(jsDriver, 10);
    jsExec = (JavascriptExecutor) jsDriver;
    log.info(this.getClass().getSimpleName() + " has been initialised.");
  }

  private void ajaxComplete() {
    String script =
        new StringBuilder()
            .append("var callback = arguments[arguments.lenght -1];")
            .append("var xhr = new XMLHttpRequest();")
            .append("xhr.open('GET','/Ajax_call', true);")
            .append("xhr.onreadystatechange = function() {")
            .append("    if(xhr.readyState == 4) {")
            .append("        callback(xhr.responseText);")
            .append("    }")
            .append("};")
            .append("xhr.send();")
            .toString();
    jsExec.executeScript(script);
  }

  public void waitForJQueryLoad() {
    try {
      ExpectedCondition<Boolean> jQueryLoad =
          webDriver ->
              ((Long) ((JavascriptExecutor) jsWait).executeScript("return jQuery.active") == 0);

      boolean jqueryReady = (Boolean) jsExec.executeScript("return jQuery.active==0");
      if (!jqueryReady) {
        jsWait.until(jQueryLoad);
      }
    } catch (WebDriverException e) {
      log.error("Error: " + e.getMessage(), e);
    }
  }

  public void waitForAngularLoad() {
    String angularReadyScript =
        "return angular.element(document).injector().get('$http').pendingRequests.length === 0";
    angularLoads(angularReadyScript);
  }

  private void angularLoads(String angularReadyScript) {
    try {
      ExpectedCondition<Boolean> angularLoad =
          webDriver ->
              ((Long) ((JavascriptExecutor) jsWait).executeScript(angularReadyScript) == 0);

      boolean jqueryReady = (Boolean) jsExec.executeScript(angularReadyScript);
      if (!jqueryReady) {
        jsWait.until(angularLoad);
      }
    } catch (WebDriverException e) {
      log.error("Error: " + e.getMessage(), e);
    }
  }
}
