package com.gitlab.rstadeu.automation.cucumber.initialize;

public class TestValues {
  private static String browser;

  public static void setBrowser(String b){browser = b;}
  public static String getBrowser(){return browser;}
}
