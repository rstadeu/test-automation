package com.gitlab.rstadeu.automation.cucumber.managers;

import com.gitlab.rstadeu.automation.cucumber.pageobjects.FacebookPage;
import org.openqa.selenium.WebDriver;

public class PageObjectManager {
    private WebDriver driver;
    private FacebookPage facebookPage;
    public PageObjectManager(WebDriver driver){
        this.driver = driver;
    }

    public FacebookPage getFacebookPage(){
        return (facebookPage == null) ? facebookPage = new FacebookPage(driver) : facebookPage;
    }
}
