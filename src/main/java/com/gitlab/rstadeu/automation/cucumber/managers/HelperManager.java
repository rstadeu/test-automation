package com.gitlab.rstadeu.automation.cucumber.managers;

import com.gitlab.rstadeu.automation.cucumber.helpers.AngularHelper;
import com.gitlab.rstadeu.automation.cucumber.helpers.JsHelper;
import com.gitlab.rstadeu.automation.cucumber.helpers.WaitHelper;
import org.openqa.selenium.WebDriver;

public class HelperManager {
  private WebDriver driver;
  private AngularHelper angularHelper;
  private JsHelper jsHelper;
  private WaitHelper waitHelper;

  public HelperManager(WebDriver driver) {
    this.driver = driver;
  }

  public JsHelper getJsHelper() {
    return (jsHelper == null) ? jsHelper = new JsHelper(driver) : jsHelper;
  }

  public AngularHelper getAngularHelper(){
      return (angularHelper == null) ? angularHelper = new AngularHelper(driver) : angularHelper;
  }

  public WaitHelper getWaitHelper(){
      return (waitHelper == null) ? waitHelper = new WaitHelper(driver) : waitHelper;
  }

}
