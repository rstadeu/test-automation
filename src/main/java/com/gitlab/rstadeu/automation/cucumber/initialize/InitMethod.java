package com.gitlab.rstadeu.automation.cucumber.initialize;

import com.gitlab.rstadeu.automation.cucumber.readers.ApplicationConfigReader;

public class InitMethod {
    public static ApplicationConfigReader appConfig = new ApplicationConfigReader();
    public static String applicationUrl = appConfig.getApplicationUrl();
    public static long implicitWait = appConfig.getImplicitlyWait();
    public static long explicitWait = appConfig.getExplicitWait();
    public static long pollingTimeInMilliSec = appConfig.getPollingTimeInMilliSec();
    public static boolean windowMaximize = appConfig.isWindowMaximize();
    public static long pageLoadTime = appConfig.getPageLoadTime();
    public static long scriptTimeOut = appConfig.getScriptTimeOut();
    public static String environmentType = appConfig.getEnvironmentType();
    public static String browser = TestValues.getBrowser();
    public static String chromeAddArguments = appConfig.getChromeAddArguments();
    public static String chromeSetExperimentalOption = appConfig.getChromeSetExperimentalOption();
    public static String windowsChromeDriver = appConfig.getWindowsChromeDriver();
    public static String windowsFireFoxDriver = appConfig.getWindowsFireFoxDriver();
    public static String windowsIEDriver = appConfig.getWindowsIEDriver();
    public static String windowsEdgeDriver = appConfig.getWindowsEdgeDriver();
    public static String windowsOperaDriver = appConfig.getWindowsOperaDriver();
    public static String linuxChromeDriver = appConfig.getLinuxChromeDriver();
    public static String linuxFireFoxDriver = appConfig.getLinuxFireFoxDriver();
    public static String linuxOperaDriver = appConfig.getLinuxOperaDriver();
    public static String macOsChromeDriver = appConfig.getMacosChromeDriver();
    public static String macOsFireFoxDriver = appConfig.getMacosFireFoxDriver();
    public static String macOsEdgeDriver = appConfig.getMacosEdgeDriver();
    public static String macOsOperaDriver = appConfig.getMacosOperaDriver();
    public static String chromeHeadless = appConfig.getChromeHeadless();
    public static String chromeDisableGpu = appConfig.getChromeDisableGpu();
    public static String chromeWindowSize = appConfig.getChromeWindowsize();
    public static String chromeIgnoreErrors = appConfig.getChromeIgnoreErrors();
    public static String seleniumHub = appConfig.getSeleniumHub();

}
