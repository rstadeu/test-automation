package com.gitlab.rstadeu.automation.cucumber.enums;

public enum EnvironmentType {
    LOCAL,
    REMOTE
}
