package com.gitlab.rstadeu.automation.cucumber.helpers;

public class ResourceHelper {
    public static String getResourcePath(String path){
        String basePath = System.getProperty("user.dir");
        return basePath + "/" + path;
    }
}
