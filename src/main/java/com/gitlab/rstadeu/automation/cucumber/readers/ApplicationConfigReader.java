package com.gitlab.rstadeu.automation.cucumber.readers;

import lombok.Getter;
import ru.qatools.properties.Property;
import ru.qatools.properties.PropertyLoader;
import ru.qatools.properties.Resource;

@Getter
@Resource.Classpath("ApplicationConfig.properties")
public class ApplicationConfigReader {
  @Property(value = "applicationUrl")
  private String applicationUrl;

  @Property(value = "implicitlyWait")
  private long implicitlyWait;

  @Property(value = "explicitWait")
  private long explicitWait;

  @Property(value = "pollingTimeInMilliSec")
  private long pollingTimeInMilliSec;

  @Property(value = "windowMaximize")
  private boolean windowMaximize;

  @Property(value = "pageLoadTime")
  private long pageLoadTime;

  @Property(value = "scriptTimeOut")
  private long scriptTimeOut;

  @Property(value = "environmentType")
  private String environmentType;

  @Property(value = "chrome.addArguments")
  private String chromeAddArguments;

  @Property(value = "chrome.setExperimentalOption")
  private String chromeSetExperimentalOption;

  @Property(value = "windows.chrome")
  private String windowsChromeDriver;

  @Property(value = "windows.firefox")
  private String windowsFireFoxDriver;

  @Property(value = "windows.internetExplorer")
  private String windowsIEDriver;

  @Property(value = "windows.edge")
  private String windowsEdgeDriver;

  @Property(value = "windows.opera")
  private String windowsOperaDriver;

  @Property(value = "linux.chrome")
  private String linuxChromeDriver;

  @Property(value = "linux.firefox")
  private String linuxFireFoxDriver;

  @Property(value = "linux.opera")
  private String linuxOperaDriver;

  @Property(value = "macos.chrome")
  private String macosChromeDriver;

  @Property(value = "macos.firefox")
  private String macosFireFoxDriver;

  @Property(value = "macos.edge")
  private String macosEdgeDriver;

  @Property(value = "macos.opera")
  private String macosOperaDriver;

  @Property(value = "chrome.headless")
  private String chromeHeadless;

  @Property(value = "chrome.disablegpu")
  private String chromeDisableGpu;

  @Property(value = "chrome.windowsize")
  private String chromeWindowsize;

  @Property(value = "chrome.ignoreerrors")
  private String chromeIgnoreErrors;

  @Property(value = "seleniumHub")
  private String seleniumHub;



  public ApplicationConfigReader() {
    PropertyLoader.newInstance().populate(this);
  }
}
