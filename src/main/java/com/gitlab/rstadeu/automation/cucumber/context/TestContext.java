package com.gitlab.rstadeu.automation.cucumber.context;

import com.gitlab.rstadeu.automation.cucumber.managers.HelperManager;
import com.gitlab.rstadeu.automation.cucumber.managers.PageObjectManager;
import com.gitlab.rstadeu.automation.cucumber.managers.DriverManager;

public class TestContext {
  private DriverManager driverManager;
  private HelperManager helperManager;
  private PageObjectManager pageObjectManager;
  private ScenarioContext scenarioContext;

  public TestContext() {
    driverManager = new DriverManager();
    helperManager = new HelperManager(driverManager.getDriver());
    pageObjectManager = new PageObjectManager(driverManager.getDriver());
    scenarioContext = new ScenarioContext();
  }

  public DriverManager getDriverManager() {
    return driverManager;
  }

  public HelperManager getHelperManager() {
    return helperManager;
  }

  public PageObjectManager getPageObjectManager() {
    return pageObjectManager;
  }

  public ScenarioContext getScenarioContext() {
    return scenarioContext;
  }
}
