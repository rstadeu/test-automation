package com.gitlab.rstadeu.automation.cucumber.helpers;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class LoggerHelper {
    public static Logger getLogger(Class cls){
        PropertyConfigurator.configure(ResourceHelper.getResourcePath("src/main/resources/log4j.properties"));
        return Logger.getLogger(cls);
    }
}
