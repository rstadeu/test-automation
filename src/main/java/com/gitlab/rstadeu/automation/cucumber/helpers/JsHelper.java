package com.gitlab.rstadeu.automation.cucumber.helpers;

import org.apache.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class JsHelper {
  private static JavascriptExecutor jsExec;
  private WebDriver driver;
  private static Logger log = LoggerHelper.getLogger(JsHelper.class);

  public JsHelper(WebDriver driver) {
    this.driver = driver;
    log.info(this.getClass().getSimpleName() + " has been initialised.");
  }

  private Object executeScript(String script) {
    JavascriptExecutor exe = (JavascriptExecutor) driver;
    return exe.executeScript(script);
  }

  private Object executeScript(String script, Object... args) {
    JavascriptExecutor exe = (JavascriptExecutor) driver;
    return exe.executeScript(script, args);
  }

  public void scrollToElement(WebElement element) {
    log.info("scroll to WebElement ...");
    executeScript(
        "windows.scrollTo(arguments[0], arguments[1]);",
        element.getLocation().x,
        element.getLocation().y);
  }

  public void scrollToElementAndClick(WebElement element) {
    scrollToElement(element);
    element.click();
    log.info("element is clicked: " + element.toString());
  }

  public void scrollIntoView(WebElement element) {
    log.info("scroll till web element");
    executeScript("arguments[0].scrollIntoView();", element);
  }

  public void clickElement(WebElement element) {
    int attempt = 0;
    while (attempt < 5) {
      try {
        executeScript("arguments[0].click();", element);
        log.info("element is clicked: " + element.toString());
        break;
      } catch (StaleElementReferenceException ignored) {
        attempt++;
      }
    }
  }
}
