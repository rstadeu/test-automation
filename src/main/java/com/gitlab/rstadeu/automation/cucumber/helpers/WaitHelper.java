package com.gitlab.rstadeu.automation.cucumber.helpers;

import com.gitlab.rstadeu.automation.cucumber.initialize.InitMethod;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;
import org.apache.log4j.Logger;

public class WaitHelper {
  private static Logger log = LoggerHelper.getLogger(WaitHelper.class);
  private WebDriver driver;

  public WaitHelper(WebDriver driver) {
    this.driver = driver;
    log.info(this.getClass().getSimpleName() + " has been initialised.");
  }

  public void setImplicitWait() {
    driver.manage().timeouts().implicitlyWait(InitMethod.implicitWait, TimeUnit.SECONDS);
  }

  private WebDriverWait getWait() {
    WebDriverWait wait = new WebDriverWait(driver, InitMethod.explicitWait);
    wait.pollingEvery(Duration.ofMillis(InitMethod.pollingTimeInMilliSec));
    wait.ignoring(NoSuchElementException.class);
    wait.ignoring(ElementNotVisibleException.class);
    wait.ignoring(StaleElementReferenceException.class);
    wait.ignoring(NoSuchFrameException.class);
    return wait;
  }

  public void waitForElement(WebElement element){
      WebDriverWait wait = getWait();
      wait.until(ExpectedConditions.visibilityOf(element));
      log.info(element.toString() + " is visible now.");
  }
}
