package com.gitlab.rstadeu.automation.cucumber.managers;

import com.gitlab.rstadeu.automation.cucumber.helpers.LoggerHelper;
import com.gitlab.rstadeu.automation.cucumber.initialize.InitMethod;
import com.paulhammant.ngwebdriver.NgWebDriver;
import lombok.NoArgsConstructor;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeDriverService;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.GeckoDriverService;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerDriverService;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.opera.OperaDriverService;
import org.openqa.selenium.opera.OperaOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

@NoArgsConstructor
public class DriverManager extends InitMethod {
  private static Logger log = LoggerHelper.getLogger(DriverManager.class);
  public static DesiredCapabilities caps = new DesiredCapabilities();
  private WebDriver driver;
  private ChromeOptions chromeOptions;
  private NgWebDriver ngWebDriver;
  private String osName = System.getProperty("os.name").toLowerCase();

  public WebDriver getDriver() {
    if (driver == null) {
      try {
        driver = createDriver();
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
    return driver;
  }

  private WebDriver createDriver() throws Exception {
    switch (environmentType.toLowerCase()) {
      case "local":
        driver = createLocalDriver();
        break;
      case "remote":
        driver = createRemoteDriver();
        break;
    }
    return driver;
  }

  private WebDriver createRemoteDriver() {
    switch (osName.substring(0, 3)) {
      case "win":
        configureDriverForWindowsRemote();
        break;
      case "mac":
        configureDriverForMacOsRemote();
        break;
      case "lin":
        configureDriverForLinuxRemote();
        break;
      default:
        break;
    }
    try {
      return new RemoteWebDriver(new URL(seleniumHub), caps);
    } catch (MalformedURLException e) {
      e.printStackTrace();
    } finally {
      Runtime.getRuntime().addShutdownHook(new Thread(new BrowserCleanup()));
    }
    deleteCookies();
    if (implicitWait > 0) {
      setImplicitlyWait(implicitWait);
    }
    if (pageLoadTime > 0) {
      setMaxLoadPageTime(pageLoadTime);
    }
    getApplicationUrl();
    return null;
  }

  private WebDriver createLocalDriver() {
    String slicedOsName = osName.substring(0, 3);
    switch (slicedOsName) {
      case "win":
        configureDriverForWindows();
        break;
      case "mac":
        configureDriverForMacOs();
        break;
      case "lin":
        configureDriverForLinuxOs();
        break;
      default:
        break;
    }
    deleteCookies();
    if (implicitWait > 0) {
      setImplicitlyWait(implicitWait);
    }
    if (pageLoadTime > 0) {
      setMaxLoadPageTime(pageLoadTime);
    }
    getApplicationUrl();
    return driver;
  }

  private void getApplicationUrl() {
    driver.get(applicationUrl);
  }

  private void setMaxLoadPageTime(long pageLoadTime) {
    driver.manage().timeouts().pageLoadTimeout(pageLoadTime, TimeUnit.SECONDS);
  }

  private void setImplicitlyWait(long implicitWait) {
    driver.manage().timeouts().implicitlyWait(implicitWait, TimeUnit.SECONDS);
  }

  private void deleteCookies() {
    driver.manage().deleteAllCookies();
  }

  public void quitDriver() {
    driver.close();
    driver.quit();
    driver = null;
  }

  public void refresh() {
    driver.navigate().refresh();
  }

  public void goTo(String url) {
    driver.navigate().to(url);
  }

  private void configureDriverForLinuxOs() {
    switch (browser.trim().toLowerCase()) {
      case "chrome":
        setUpChromeDriver(linuxChromeDriver);
        break;
      case "chrome_headless":
        setUpHeadlessChromeDriver(linuxChromeDriver);
        break;
      case "firefox":
        System.setProperty("webdriver.gecko.driver", linuxFireFoxDriver);
        driver = new FirefoxDriver();
        break;
      case "opera":
        System.setProperty(OperaDriverService.OPERA_DRIVER_EXE_PROPERTY, linuxOperaDriver);
        driver = new OperaDriver();
        break;
      default:
        setUpChromeDriver(windowsChromeDriver);
        break;
    }
  }

  private void configureDriverForMacOs() {
    switch (browser.trim().toLowerCase()) {
      case "chrome":
        setUpChromeDriver(macOsChromeDriver);
        break;
      case "chrome_headless":
        setUpHeadlessChromeDriver(macOsChromeDriver);
        break;
      case "firefox":
        System.setProperty("webdriver.gecko.driver", macOsFireFoxDriver);
        driver = new FirefoxDriver();
        break;
      case "edge":
        System.setProperty(EdgeDriverService.EDGE_DRIVER_EXE_PROPERTY, macOsEdgeDriver);
        driver = new EdgeDriver();
        break;
      case "opera":
        System.setProperty(OperaDriverService.OPERA_DRIVER_EXE_PROPERTY, macOsOperaDriver);
        driver = new OperaDriver();
        break;
      default:
        setUpChromeDriver(windowsChromeDriver);
        break;
    }
  }

  private void configureDriverForWindows() {
    switch (browser.trim().toLowerCase()) {
      case "chrome":
        setUpChromeDriver(windowsChromeDriver);
        break;
      case "chrome_headless":
        setUpHeadlessChromeDriver(windowsChromeDriver);
        break;
      case "firefox":
        System.setProperty("webdriver.gecko.driver", windowsFireFoxDriver);
        driver = new FirefoxDriver();
        break;
      case "ie":
      case "internet explorer":
        System.setProperty(InternetExplorerDriverService.IE_DRIVER_EXE_PROPERTY, windowsIEDriver);
        driver = new InternetExplorerDriver();
        break;
      case "edge":
        System.setProperty(EdgeDriverService.EDGE_DRIVER_EXE_PROPERTY, windowsEdgeDriver);
        driver = new EdgeDriver();
        break;
      case "opera":
        System.setProperty(OperaDriverService.OPERA_DRIVER_EXE_PROPERTY, windowsOperaDriver);
        driver = new OperaDriver();
        break;
      default:
        setUpChromeDriver(windowsChromeDriver);
        break;
    }
  }

  private void configureDriverForWindowsRemote() {
    switch (browser.trim().toLowerCase()) {
      case "chrome":
        setUpChromeDriverRemote(windowsChromeDriver);
        break;
      case "chrome_headless":
        setUpHeadlessChromeDriverRemote(windowsChromeDriver);
        break;
      case "firefox":
        caps = DesiredCapabilities.firefox();
        caps.setJavascriptEnabled(true);
        GeckoDriverService service =
            new GeckoDriverService.Builder()
                .usingFirefoxBinary(new FirefoxBinary(new File(windowsFireFoxDriver)))
                .build();
        FirefoxOptions options = new FirefoxOptions();
        options.merge(caps);
        driver = new FirefoxDriver(service, options);
        break;
      case "ie":
      case "internet explorer":
        caps = DesiredCapabilities.internetExplorer();
        caps.setJavascriptEnabled(true);
        InternetExplorerDriverService serviceIe =
            new InternetExplorerDriverService.Builder()
                .usingDriverExecutable(new File(windowsIEDriver))
                .build();
        InternetExplorerOptions optionsIe = new InternetExplorerOptions();
        optionsIe.merge(caps);
        driver = new InternetExplorerDriver(serviceIe, optionsIe);
        break;
      case "edge":
        caps = DesiredCapabilities.edge();
        caps.setJavascriptEnabled(true);
        EdgeDriverService edgeDriverService =
            new EdgeDriverService.Builder()
                .usingDriverExecutable(new File(windowsEdgeDriver))
                .build();
        EdgeOptions edgeOptions = new EdgeOptions();
        edgeOptions.merge(caps);
        driver = new EdgeDriver(edgeDriverService, edgeOptions);
        break;
      case "opera":
        caps = DesiredCapabilities.operaBlink();
        caps.setJavascriptEnabled(true);
        OperaOptions operaOptions = new OperaOptions();
        operaOptions.merge(caps);
        OperaDriverService operaDriverService =
            new OperaDriverService.Builder()
                .usingDriverExecutable(new File(windowsOperaDriver))
                .build();
        driver = new OperaDriver(operaDriverService, operaOptions);
        break;
      default:
        setUpChromeDriverRemote(windowsChromeDriver);
        break;
    }
  }

  private void configureDriverForMacOsRemote() {
    switch (browser.trim().toLowerCase()) {
      case "chrome":
        setUpChromeDriverRemote(macOsChromeDriver);
        break;
      case "chrome_headless":
        setUpHeadlessChromeDriverRemote(macOsChromeDriver);
        break;
      case "firefox":
        caps = DesiredCapabilities.firefox();
        caps.setJavascriptEnabled(true);
        GeckoDriverService service =
            new GeckoDriverService.Builder()
                .usingFirefoxBinary(new FirefoxBinary(new File(macOsFireFoxDriver)))
                .build();
        FirefoxOptions options = new FirefoxOptions();
        options.merge(caps);
        driver = new FirefoxDriver(service, options);
        break;
      case "edge":
        caps = DesiredCapabilities.edge();
        caps.setJavascriptEnabled(true);
        EdgeDriverService edgeDriverService =
            new EdgeDriverService.Builder()
                .usingDriverExecutable(new File(macOsEdgeDriver))
                .build();
        EdgeOptions edgeOptions = new EdgeOptions();
        edgeOptions.merge(caps);
        driver = new EdgeDriver(edgeDriverService, edgeOptions);
        break;
      case "opera":
        caps = DesiredCapabilities.operaBlink();
        caps.setJavascriptEnabled(true);
        OperaOptions operaOptions = new OperaOptions();
        operaOptions.merge(caps);
        OperaDriverService operaDriverService =
            new OperaDriverService.Builder()
                .usingDriverExecutable(new File(macOsOperaDriver))
                .build();
        driver = new OperaDriver(operaDriverService, operaOptions);
        break;
      default:
        setUpChromeDriverRemote(windowsChromeDriver);
        break;
    }
  }

  private void configureDriverForLinuxRemote() {
    switch (browser.trim().toLowerCase()) {
      case "chrome":
        setUpChromeDriverRemote(linuxChromeDriver);
        break;
      case "chrome_headless":
        setUpHeadlessChromeDriverRemote(linuxChromeDriver);
        break;
      case "firefox":
        caps = DesiredCapabilities.firefox();
        caps.setJavascriptEnabled(true);
        GeckoDriverService service =
            new GeckoDriverService.Builder()
                .usingFirefoxBinary(new FirefoxBinary(new File(linuxFireFoxDriver)))
                .build();
        FirefoxOptions options = new FirefoxOptions();
        options.merge(caps);
        driver = new FirefoxDriver(service, options);
        break;
      case "opera":
        caps = DesiredCapabilities.operaBlink();
        caps.setJavascriptEnabled(true);
        OperaOptions operaOptions = new OperaOptions();
        operaOptions.merge(caps);
        OperaDriverService operaDriverService =
            new OperaDriverService.Builder()
                .usingDriverExecutable(new File(linuxOperaDriver))
                .build();
        driver = new OperaDriver(operaDriverService, operaOptions);
        break;
      default:
        setUpChromeDriverRemote(windowsChromeDriver);
        break;
    }
  }

  private void setUpHeadlessChromeDriver(String chromeDriverPath) {
    System.setProperty(ChromeDriverService.CHROME_DRIVER_EXE_PROPERTY, chromeDriverPath);
    chromeOptions = new ChromeOptions();
    chromeOptions.addArguments(
        chromeHeadless, chromeDisableGpu, chromeWindowSize, chromeIgnoreErrors);
    driver = new ChromeDriver(chromeOptions);
  }

  private void setUpChromeDriver(String chromeDriverPath) {
    System.setProperty(ChromeDriverService.CHROME_DRIVER_EXE_PROPERTY, chromeDriverPath);
    chromeOptions = new ChromeOptions();
    chromeOptions.addArguments(chromeAddArguments);
    chromeOptions.addArguments("--disable-gpu");
    chromeOptions.setExperimentalOption(chromeSetExperimentalOption, false);
    driver = new ChromeDriver(chromeOptions);
  }

  private void setUpChromeDriverRemote(String chromeDriverPath) {
    System.setProperty(ChromeDriverService.CHROME_DRIVER_EXE_PROPERTY, chromeDriverPath);
    chromeOptions = new ChromeOptions();
    caps.setJavascriptEnabled(true);
    chromeOptions.setCapability(ChromeOptions.CAPABILITY, caps);
    chromeOptions.addArguments(chromeAddArguments);
    chromeOptions.setExperimentalOption(chromeSetExperimentalOption, false);
    driver = new ChromeDriver(chromeOptions);
  }

  private void setUpHeadlessChromeDriverRemote(String chromeDriverPath) {
    System.setProperty(ChromeDriverService.CHROME_DRIVER_EXE_PROPERTY, chromeDriverPath);
    chromeOptions = new ChromeOptions();
    caps.setJavascriptEnabled(true);
    chromeOptions.setCapability(ChromeOptions.CAPABILITY, caps);
    chromeOptions.addArguments(
        chromeHeadless, chromeDisableGpu, chromeWindowSize, chromeIgnoreErrors);
    driver = new ChromeDriver(chromeOptions);
  }

  private class BrowserCleanup implements Runnable {
    public void run() {
      log.info("Cleaning up the browser");
      try {
        driver.quit();
      } catch (NullPointerException e) {
        log.info("Browser already shut down");
      }
    }
  }
}
