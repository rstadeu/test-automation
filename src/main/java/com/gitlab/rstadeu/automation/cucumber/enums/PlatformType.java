package com.gitlab.rstadeu.automation.cucumber.enums;

public enum PlatformType {
  WINDOWS,
  LINUX,
  MAC,
  ANDROID
}
