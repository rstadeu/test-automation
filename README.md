# Automation Framework with Cucumber and Selenium

This is a Selenium Cucumber BDD Framework.
- Written in **Java**
- Implemented using **TestNG** and supports **JUnit** as well
- Build Tool: **Maven**
- Implemented **Page Object Model** Design Pattern
- SeleniumGrid - Parallel Execution
- test-suites - Running tests Locally and Remotely
---
### Browsers Support
- Google Chrome
- Internet Explorer
- Edge
- Mozilla Firefox
- Opera
---
### Headless Support
- Chrome Headless
- PhantonJs
---
### Platform Support
- Windows
- Linux 
- MacOs
---
### Installation
$ `git clone https://gitlab.com/rstadeu/test-automation.git `\
$ `mvn clean test` 
---
### Browser Setup
Uses Maven to invoke different browsers
#### Running Local

##### Chrome
`mvn clean test -DsuiteXmlFile=test-suites/testng-local.xml -Dbrowser=chrome`
##### Internet Explorer
`mvn clean test -DsuiteXmlFile=test-suites/testng-local.xml -Dbrowser=ie`
##### Edge
`mvn clean test -DsuiteXmlFile=test-suites/testng-local.xml -Dbrowser=edge`
##### Mozilla Firefox
`mvn clean test -DsuiteXmlFile=test-suites/testng-local.xml -Dbrowser=firefox`
##### Opera
`mvn clean test -DsuiteXmlFile=test-suites/testng-local.xml -Dbrowser=opera`
##### Chrome Headless
`mvn clean test -DsuiteXmlFile=test-suites/testng-local.xml -Dbrowser=chrome_headless`
##### PhantomJs
`mvn clean test -DsuiteXmlFile=test-suites/testng-local.xml -Dbrowser=phatomjs`

#### Running Remotely

##### Chrome
`mvn clean test -DsuiteXmlFile=test-suites/testng-remote.xml -Dbrowser=chrome`
##### Internet Explorer
`mvn clean test -DsuiteXmlFile=test-suites/testng-remote.xml -Dbrowser=ie`
##### Edge
`mvn clean test -DsuiteXmlFile=test-suites/testng-remote.xml -Dbrowser=edge`
##### Mozilla Firefox
`mvn clean test -DsuiteXmlFile=test-suites/testng-remote.xml -Dbrowser=firefox`
##### Opera
`mvn clean test -DsuiteXmlFile=test-suites/testng-remote.xml -Dbrowser=opera`
##### Chrome Headless
`mvn clean test -DsuiteXmlFile=test-suites/testng-remote.xml -Dbrowser=chrome_headless`
##### PhantomJs
`mvn clean test -DsuiteXmlFile=test-suites/testng-remote.xml -Dbrowser=phatomjs`

---

### Core Classes
ApplicationConfigReader\
DriverManager\
PageObjectManager\
DataFileManager\
HelpManager

---
### TestNG
testng-local.xml\
testng-remote.xml
---
### Report and Screenshots Details
Reports and Screenshots will be generated in project target folder\
![Report Result](/src/test/resources/data/reportCucumber.png)